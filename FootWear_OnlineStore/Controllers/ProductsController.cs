﻿using FootWear_OnlineStore.Models;
using FootWear_OnlineStore.OSDB;
using FootWear_OnlineStore.ViewModels;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using System.Web.Routing;
using PagedList;

namespace FootWear_OnlineStore.Controllers
{
    [Authorize(Roles = "Admin")]
    public class ProductsController : Controller
    {
        private StoreContext db = new StoreContext();

        // GET: Products
        [AllowAnonymous]
        public ActionResult Index(string category, string search, string sortBy, int? page)
        {
            //Instantiate a new view model      
            // Data migration and seeding enabled (at CompoundKeysProductColoursAndSizes) 
            // Added 9 Jan 2019 -  Sorting by prices, and paging at products index.
            // Lab05 completed 9 Jan 2019
            // Added 11 Jan 2019 - RolesManager with Admin user and "two context" database migrations with "StoreConfiguration" for products and store 
            // Addedd 12 Jan 2019 ProductImage table and multiple image uploading action ProductImages Controller.
            // Added 13 jan 2019: associate image to products and productcontroller update 
            // Added 14 Jan 2019: routing maps for Products index with category filtering and page # in RoutesConfig (As done in Lab06a)
            // Added 15 Jan 2019: Lab06b check for image upload, Lab06c check for multiple image uploads. Added Custom error page for over 20MB
            //                    Note: 15 Jan 2019 - added Lab06c the check for  request length and a custom error message ( see Chapter Managing Images )
            // Added 16 Jan 2019: Associate images to products with the images being able to belong to more than one product as in Lab06d
            //                    Created partial view for editing and creating products.
            // Added 16 Jan 2019: Fixing the Navigation Bar Style Issues as in Chapter Authentication and Authorisation to fix page header.
            // Added 17 Jan 2019: Admin ViewModels for Roles and RolesAdminController and AdminController
            // Added 17 Jan 2019: Updated Identity user with Name, Address and date of birth fields. 
            //                    Migrate DApplicationDbContext using named context "Configuration"
            // Added 17 Jan 2019: UsersAdminController as in Chapter Authentication and Authorisation
            // Added 17 Jan 2019: User self registration and user view and edit their details. Allow Users to Reset Their Passwords
            // Added 17 Jan 2019: Authorization for Product and Category Administration and friendlier ReturnUrl redirection for Login and Register user.
            // Added 20 Jan 2019: Baskets model with Model Binding to a List or an Array, and Basket item removal/update
            // Added 21 Jan 2019: Basket Summary and Migrating a Basket When a User Logs In or Registers (see AccountController, Login action.)
            // Added 27 Jan 2019: Ref: Chapter 9 - Add Order and OrderLines, View Orders and Displaying Order Data.
            //                    Place an order and Create Order from basket review. Saving an Order to the Database.
            //                    Updating Product Deletion to Avoid Foreign Key Conflicts.
            //                    Searching Orders to filter by search text. Sorting Orders in index page.
            //                    Made Home page with Best selling shoes (top 4).
            // Added 28 Jan 2019: Tutorial 8 CSS Usage in online store. Ref:Chapter 14 Stylesheets store.css and reset.css added for new styling of web site. 
            // Added 4 Feb 2019: Top Image and seach box for site and navigation styles (as in Chapter 17)
            // Added 8 Feb 2019: Responsize styles for smaller devices. Tidy up the buttons and tables for small devices.

            ProductIndexViewModel viewModel = new ProductIndexViewModel(); 
            var products = db.Products.Include(p => p.Category);

            // Filter product list to seach
            if (!String.IsNullOrEmpty(search))
            {
                products = products.Where(p => p.Name.Contains(search) ||
                    p.Description.Contains(search) ||
                    p.Category.Name.Contains(search) );
                viewModel.Search = search;
            }

            viewModel.CatsWithCount = from matchingProducts in products
                                      where matchingProducts.CategoryID != null
                                      group matchingProducts by
                                      matchingProducts.Category.Name into catGroup
                                      select new CategoryWithCount()
                                      {
                                          CategoryName = catGroup.Key,
                                          ProductCount = catGroup.Count()
                                      };

            if (!String.IsNullOrEmpty(category)) {
                products = products.Where(p => p.Category.Name == category);
                viewModel.Category = category; // Added 9 Jan 2019 for PagedList to remember category in pages
            }

            // Added: Rodney 9 Jan 2019 sort the results
            switch (sortBy)
            {
                case "price_lowest":
                    products = products.OrderBy(p => p.Price);
                    break;
                case "price_highest":
                    products = products.OrderByDescending(p => p.Price);
                    break;
                default:
                    // Added 9 Jan 2019 for PagedList as default order by
                    products = products.OrderBy(p => p.Name);
                    break;
            }
            int currentPage = (page ?? 1);
            viewModel.Products = products.ToPagedList(currentPage, Constants.PageItems);
            viewModel.SortBy = sortBy; // Added 9 Jan 2019 for PagedList to remember the sorted by 

            // Added: Rodney 9 Jan 2019 sort the results
            viewModel.Sorts = new Dictionary<string, string>
            {
                {"Price low to high", "price_lowest" },
                {"Price high to low", "price_highest" }
            };


            return View(viewModel);
        }

        [AllowAnonymous]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Product product = db.Products.Find(id);
            if (product == null)
            {
                return HttpNotFound();
            }
            ProductViewModel viewModel = new ProductViewModel();
            viewModel.ID = product.ID;
            viewModel.Product = product;
            productColourAndSizeToVM(viewModel, product.ID);
            return View(viewModel);
            //return View(product);
        }

        // GET: Products/Create
        public ActionResult Create()
        {
            // Added 13 Jan 2019 - use ProductViewModel
            ProductViewModel viewModel = new ProductViewModel();
            viewModel.CategoryList = new SelectList(db.Categories, "ID", "Name");
            viewModel.ImageLists = new List<SelectList>();
            for (int i = 0; i < Constants.NumberOfProductImages; i++)
            {
                viewModel.ImageLists.Add(new SelectList(db.ProductImages, "ID", "FileName"));
            }
            return View(viewModel);
        }

        // POST: Products/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(ProductViewModel viewModel)
        {
            Product product = new Product();
            product.Name = viewModel.Name;
            product.Description = viewModel.Description;
            product.Price = viewModel.Price;
            product.CategoryID = viewModel.CategoryID;
            product.ProductImageMappings = new List<ProductImageMapping>();
            //get a list of selected images without any blanks
            string[] productImages = viewModel.ProductImages.Where(pi =>
                !string.IsNullOrEmpty(pi)).ToArray();
            for (int i = 0; i < productImages.Length; i++)
            {
                product.ProductImageMappings.Add(new ProductImageMapping
                {
                    ProductImage = db.ProductImages.Find(int.Parse(productImages[i])),
                    ImageNumber = i
                });
            }
            if (ModelState.IsValid)
            {
                db.Products.Add(product);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            viewModel.CategoryList = new SelectList(db.Categories, "ID", "Name", product.CategoryID);
            viewModel.ImageLists = new List<SelectList>();
            for (int i = 0; i < Constants.NumberOfProductImages; i++)
            {
                viewModel.ImageLists.Add(new SelectList(db.ProductImages, "ID", "FileName",
                        viewModel.ProductImages[i]));
            }
            return View(viewModel);
        }

        // GET: Products/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Product product = db.Products.Find(id);
            if (product == null)
            {
                return HttpNotFound();
            }
 
            ProductViewModel viewModel = new ProductViewModel();
            viewModel.CategoryList = new SelectList(db.Categories, "ID", "Name", product.CategoryID);
            viewModel.ImageLists = new List<SelectList>();
            foreach (var imageMapping in product.ProductImageMappings.OrderBy(pim => pim.ImageNumber))
            {
                viewModel.ImageLists.Add(new SelectList(db.ProductImages, "ID", "FileName",
                imageMapping.ProductImageID));
            }
            for (int i = viewModel.ImageLists.Count; i < Constants.NumberOfProductImages; i++)
            {
                viewModel.ImageLists.Add(new SelectList(db.ProductImages, "ID", "FileName"));
            }
            viewModel.ID = product.ID;
            viewModel.Name = product.Name;
            viewModel.Description = product.Description;
            viewModel.Price = product.Price;
            // For Colours and Size add product with method
            //productColourAndSizeToVM(viewModel, product.ID);
            return View(viewModel);
        }

        // POST: Products/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(ProductViewModel viewModel)
        {
            var productToUpdate = db.Products.Include(p => p.ProductImageMappings).Where(p => p.ID ==
                viewModel.ID).Single();
            if (TryUpdateModel(productToUpdate, "", new string[] { "Name", "Description", "Price", "CategoryID" }))
            {
                if (productToUpdate.ProductImageMappings == null)
                {
                    productToUpdate.ProductImageMappings = new List<ProductImageMapping>();
                }
                //get a list of selected images without any blanks
                string[] productImages = viewModel.ProductImages.Where(pi =>
                    !string.IsNullOrEmpty(pi)).ToArray();
                for (int i = 0; i < productImages.Length; i++)
                {
                    //get the image currently stored
                    var imageMappingToEdit = productToUpdate.ProductImageMappings.Where(pim => pim.ImageNumber == i).FirstOrDefault();
                    //find the new image
                    var image = db.ProductImages.Find(int.Parse(productImages[i]));
                    //if there is nothing stored then we need to add a new mapping
                    if (imageMappingToEdit == null)
                    {
                        //add image to the imagemappings
                        productToUpdate.ProductImageMappings.Add(new ProductImageMapping
                        {
                            ImageNumber = i,
                            ProductImage = image,
                            ProductImageID = image.ID
                        });
                    }
                    //else it's not a new file so edit the current mapping
                    else
                    {
                        //if they are not the same
                        if (imageMappingToEdit.ProductImageID != int.Parse(productImages[i]))
                        {
                            //assign image property of the image mapping
                            imageMappingToEdit.ProductImage = image;
                        }
                    }
                }
                //delete any other imagemappings that the user did not include in their
                //selections for the product
                for (int i = productImages.Length; i < Constants.NumberOfProductImages; i++)
                {
                    var imageMappingToEdit = productToUpdate.ProductImageMappings.Where(pim =>
                        pim.ImageNumber == i).FirstOrDefault();
                    //if there is something stored in the mapping
                    if (imageMappingToEdit != null)
                    {
                        //delete the record from the mapping table directly.
                        //just calling productToUpdate.ProductImageMappings.Remove(imageMappingToEdit)  
                        //results in a FK error
                        db.ProductImageMappings.Remove(imageMappingToEdit);
                    }
                }
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(viewModel);
            //if (ModelState.IsValid)
            //{
            //    db.Entry(product).State = EntityState.Modified;
            //    db.SaveChanges();
            //    return RedirectToAction("Index");
            //}


            //ViewBag.CategoryID = new SelectList(db.Categories, "ID", "Name", product.CategoryID);
            //// Add the list ofcolours and sizes
            //ProductViewModel viewModel = new ProductViewModel();
            //viewModel.Product = product;
            //productColourAndSize(product, viewModel);
            //return View(viewModel);
        }


        // GET: Products/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Product product = db.Products.Find(id);
            if (product == null)
            {
                return HttpNotFound();
            }
            return View(product);
        }

        // POST: Products/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Product product = db.Products.Find(id);
            db.Products.Remove(product);
            var orderLines = db.OrderLines.Where(ol => ol.ProductID == id);
            foreach (var ol in orderLines)
            {
                ol.ProductID = null;
            }
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        // GET: Products/Colours/5
        public ActionResult Colours(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Product product = db.Products.Find(id);
            if (product == null)
            {
                return HttpNotFound();
            }

            ViewBag.ProductID = id;
            var queryColours = from item in db.ProductColours
                               where item.ProductID == id
                               orderby item.Colour.Name
                               select item;
            if (queryColours.Count() == 0)
            {
                var routeInfo = new RouteValueDictionary
                {
                    { "id", id }
                };
                return RedirectToAction("AddColour", routeInfo);
            }
            return View(queryColours.ToList());
        }

        // GET: Products/Sizes/5
        public ActionResult Sizes(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Product product = db.Products.Find(id);
            if (product == null)
            {
                return HttpNotFound();
            }

            ViewBag.ProductID = id;
            var querySizes = from item in db.ProductSizes
                               where item.ProductID == id
                               orderby item.Size.Region, item.Size.Measure
                               select item;
            if (querySizes.Count() == 0)
            {
                var routeInfo = new RouteValueDictionary
                {
                    { "id", id }
                };
                return RedirectToAction("AddSize", routeInfo);
            }
            return View(querySizes.ToList());
        }

        // GET: Products/AddColour/5
        public ActionResult AddColour(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Product product = db.Products.Find(id);
            if (product == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ProductViewModel viewModel = new ProductViewModel();
            viewModel.Product = product;

            // Added: Rodney 9 Jab 2019 - Remove from this colour list that product id has now.
            //viewModel.Colours = new SelectList(db.Colours, "ID", "Name");
            viewModel.Colours = (from colour in db.Colours
                               let current_colours = from item in db.ProductColours
                                                   where item.ProductID == id
                                                   select item.ColourID
                               where current_colours.Contains(colour.ID) == false
                               orderby colour.Name
                               select new SelectListItem
                               {
                                   Text = colour.Name,
                                   Value = colour.ID.ToString()
                               }).ToList();

            viewModel.ProductColour = new ProductColour();
            return View(viewModel);
        }

        // GET: Products/AddSize/5
        public ActionResult AddSize(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Product product = db.Products.Find(id);
            if (product == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ProductViewModel viewModel = new ProductViewModel();
            viewModel.Product = product;

            // Added: Rodney 9 Jab 2019 - Remove from this size list that product id has now.
            viewModel.Sizes = (from size in db.Sizes
                               let current_sizes = from item in db.ProductSizes where item.ProductID == id
                                                select item.SizeID
                               where current_sizes.Contains(size.ID) == false
                               orderby size.Region, size.Measure
                               select new SelectListItem
                                      {
                                          Text = size.Region + " " + size.Measure.ToString(),
                                          Value = size.ID.ToString()
                                      }).ToList();

            viewModel.ProductSize = new ProductSize();
            return View(viewModel);
        }

        // POST: Products/AddColour
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AddColour([Bind(Include = "ProductID,ColourID")] ProductColour productColour)
        {
            var find = db.ProductColours.Find(productColour.ProductID, productColour.ColourID);
            if (find == null) {
                if (ModelState.IsValid)
                {
                    if (productColour == null)
                    {
                        return HttpNotFound();
                    }
                    db.ProductColours.Add(productColour);
                    db.SaveChanges();
                }
            }
            var routeInfo = new RouteValueDictionary
                {
                    { "id", productColour.ProductID }
                };

            return RedirectToAction("Colours", routeInfo);
        }

        // POST: Products/AddSize
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AddSize([Bind(Include = "ProductID,SizeID")] ProductSize productSize)
        {
            var find = db.ProductSizes.Find(productSize.ProductID, productSize.SizeID);
            if (find == null)
            {
                if (ModelState.IsValid)
                {
                    db.ProductSizes.Add(productSize);
                    db.SaveChanges();
                }
            }
            var routeInfo = new RouteValueDictionary
                {
                    { "id", productSize.ProductID }
                };
            return RedirectToAction("Sizes", routeInfo);
        }

        // GET: Products/DeleteColour/5?colour=3
        public ActionResult DeleteColour(int? id, int? colour)
        {
            if (id == null || colour == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ProductColour productColour = db.ProductColours.Find(id, colour);
            if (productColour == null)
            {
                return HttpNotFound();
            }
            db.ProductColours.Remove(productColour);
            db.SaveChanges();
            var routeInfo = new RouteValueDictionary
                {
                    { "id", id }
                };
            return RedirectToAction("Colours", routeInfo);
        }
 
        // GET: Products/DeleteColour/5?colour=3
        public ActionResult DeleteSize(int? id, int? size)
        {
            if (id == null || size == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ProductSize productSize = db.ProductSizes.Find(id, size);
            if (productSize == null)
            {
                return HttpNotFound();
            }
            db.ProductSizes.Remove(productSize);
            db.SaveChanges();
            var routeInfo = new RouteValueDictionary
                {
                    { "id", id }
                };
            return RedirectToAction("Sizes", routeInfo);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        /*
         * Product colour and sizes for editing and details
         */
        private void productColourAndSizeToVM(ProductViewModel viewModel, int ID)
        {
            viewModel.ProductColours = (from colour in db.ProductColours
                                        where colour.ProductID == ID
                                        orderby colour.Colour.Name
                                        select new SelectListItem
                                        {
                                            Text = colour.Colour.Name,
                                            Value = colour.ColourID.ToString()
                                        }).ToList();
            viewModel.ProductSizes = (from size in db.ProductSizes
                                      where size.ProductID == ID
                                      orderby size.Size.Region, size.Size.Measure
                                      select new SelectListItem
                                      {
                                          Text = size.Size.Region + " " + size.Size.Measure.ToString(),
                                          Value = size.SizeID.ToString()
                                      }).ToList();
        }

    }
}
