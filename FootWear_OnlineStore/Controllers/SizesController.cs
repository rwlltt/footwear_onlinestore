﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using FootWear_OnlineStore.Models;
using FootWear_OnlineStore.OSDB;

namespace FootWear_OnlineStore.Controllers
{
    [Authorize(Roles = "Admin")]
    public class SizesController : Controller
    {
        private StoreContext db = new StoreContext();

        // GET: Sizes
        [AllowAnonymous]
        public ActionResult Index()
        {
            return View(db.Sizes.OrderBy(s => s.Region).ThenBy(s => s.Measure).ToList());
        }

        // GET: Sizes/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Size shoeSize = db.Sizes.Find(id);
            if (shoeSize == null)
            {
                return HttpNotFound();
            }
            return View(shoeSize);
        }

        // GET: Sizes/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Sizes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,Region,Measure")] Size shoeSize)
        {
            if (ModelState.IsValid)
            {
                db.Sizes.Add(shoeSize);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(shoeSize);
        }

        // GET: Sizes/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Size shoeSize = db.Sizes.Find(id);
            if (shoeSize == null)
            {
                return HttpNotFound();
            }
            return View(shoeSize);
        }

        // POST: Sizes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,Region,Measure")] Size shoeSize)
        {
            if (ModelState.IsValid)
            {
                db.Entry(shoeSize).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(shoeSize);
        }

        // GET: Sizes/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Size shoeSize = db.Sizes.Find(id);
            if (shoeSize == null)
            {
                return HttpNotFound();
            }
            return View(shoeSize);
        }

        // POST: Sizes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Size shoeSize = db.Sizes.Find(id);
            db.Sizes.Remove(shoeSize);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
