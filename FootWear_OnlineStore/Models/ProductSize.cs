﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FootWear_OnlineStore.Models
{
    public partial class ProductSize
    {
        [Key]
        [Column(Order = 1)]
        public int ProductID { get; set; }
        [Key]
        [Column(Order = 2)]
        public int SizeID { get; set; }
        public virtual Product Product { get; set; }
        public virtual Size Size { get; set; }
    }
}