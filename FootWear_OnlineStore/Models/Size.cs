﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FootWear_OnlineStore.Models
{
    public partial class Size
    {
        public int ID { get; set; }
        public string Region { get; set; }
        public int Measure { get; set; }
        public virtual ICollection<ProductSize> Products { get; set; }
    }
}