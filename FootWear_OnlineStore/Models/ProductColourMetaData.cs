﻿using System.ComponentModel.DataAnnotations;

namespace FootWear_OnlineStore.Models
{
    [MetadataType(typeof(ProductColourMetaData))]
    public partial class ProductColour { }

    public class ProductColourMetaData
    {
        [Display(Name = "Colour Name")]
        public int ColourID { get; set; }
    }
}