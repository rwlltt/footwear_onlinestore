﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FootWear_OnlineStore.Models
{
    public partial class Colour
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public virtual ICollection<ProductColour> Products { get; set; }
    }
}