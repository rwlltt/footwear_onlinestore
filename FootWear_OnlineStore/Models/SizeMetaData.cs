﻿using System.ComponentModel.DataAnnotations;

namespace FootWear_OnlineStore.Models
{
    [MetadataType(typeof(SizeMetaData))]
    public partial class Size { }

    public class SizeMetaData
    {
        [Required(ErrorMessage = "The region cannot be blank")]
        [StringLength(4, MinimumLength = 2, ErrorMessage = "Please enter a region name between 2 and 4 characters in length")]
        [RegularExpression(@"^[A-Z]+$", ErrorMessage = "Please enter a region code with capital letters only")]
        [Display(Name = "Region")]
        public string Region { get; set; }
        [Required(ErrorMessage = "The measure name cannot be blank")]
        [RegularExpression(@"^[0-9]+$", ErrorMessage = "Please enter a size measure made up of numbers only")]
        [Display(Name = "Measure")]
        public int Measure { get; set; }
    }
}