﻿using System.ComponentModel.DataAnnotations;

namespace FootWear_OnlineStore.Models
{
    [MetadataType(typeof(ColourMetaData))]
    public partial class Colour { }

    public class ColourMetaData
    {
        [Required(ErrorMessage = "The colour name cannot be blank")]
        [StringLength(50, MinimumLength = 3, ErrorMessage = "Please enter a colour name between 3 and 50 characters in length")]
        [RegularExpression(@"^[a-zA-Z'-'\s]+\/[a-zA-Z'-'\s]+$", ErrorMessage = "Please enter a colour name made up two colours separated with '/' only")]
        [Display(Name = "Colour Name")]
        public string Name { get; set; }
    }
}