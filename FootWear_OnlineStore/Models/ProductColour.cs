﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FootWear_OnlineStore.Models
{
    public partial class ProductColour
    {
        [Key]
        [Column(Order = 1)]
        public int ProductID { get; set; }
        [Key]
        [Column(Order = 2)]
        public int ColourID { get; set; }
        public virtual Product Product { get; set; }
        public virtual Colour Colour { get; set; }
    }
}