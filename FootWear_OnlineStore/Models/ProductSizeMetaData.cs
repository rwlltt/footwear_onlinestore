﻿using System.ComponentModel.DataAnnotations;

namespace FootWear_OnlineStore.Models
{
    [MetadataType(typeof(ProductSizeMetaData))]
    public partial class ProductSize { }

    public class ProductSizeMetaData
    {
        [Display(Name = "Size Measure")]
        public int SizeID { get; set; }

    }
}