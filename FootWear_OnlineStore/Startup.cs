﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(FootWear_OnlineStore.Startup))]
namespace FootWear_OnlineStore
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
