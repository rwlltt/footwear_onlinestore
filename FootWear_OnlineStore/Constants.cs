﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FootWear_OnlineStore
{
    // Added: 9 Jan 2019 - constant values for the web site
    public static class Constants
    {
        public const int PageItems = 3; // PagedList number of item a page
        public const string ProductImagePath = "~/Content/ProductImages/";
        public const string ProductThumbnailPath = "~/Content/ProductImages/Thumbnails/";
        public const int NumberOfProductImages = 5; // Added 13 Jan 2019 - numbers of images for product
    }
}