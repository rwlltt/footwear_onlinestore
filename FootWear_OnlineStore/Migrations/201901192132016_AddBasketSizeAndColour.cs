namespace FootWear_OnlineStore.Migrations.StoreConfiguration
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddBasketSizeAndColour : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.BasketLines", "Colour_ID", "dbo.Colours");
            DropForeignKey("dbo.BasketLines", "Size_ID", "dbo.Sizes");
            DropIndex("dbo.BasketLines", new[] { "Colour_ID" });
            DropIndex("dbo.BasketLines", new[] { "Size_ID" });
            RenameColumn(table: "dbo.BasketLines", name: "Colour_ID", newName: "ColourID");
            RenameColumn(table: "dbo.BasketLines", name: "Size_ID", newName: "SizeID");
            AlterColumn("dbo.BasketLines", "ColourID", c => c.Int(nullable: false));
            AlterColumn("dbo.BasketLines", "SizeID", c => c.Int(nullable: false));
            CreateIndex("dbo.BasketLines", "SizeID");
            CreateIndex("dbo.BasketLines", "ColourID");
            AddForeignKey("dbo.BasketLines", "ColourID", "dbo.Colours", "ID", cascadeDelete: true);
            AddForeignKey("dbo.BasketLines", "SizeID", "dbo.Sizes", "ID", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.BasketLines", "SizeID", "dbo.Sizes");
            DropForeignKey("dbo.BasketLines", "ColourID", "dbo.Colours");
            DropIndex("dbo.BasketLines", new[] { "ColourID" });
            DropIndex("dbo.BasketLines", new[] { "SizeID" });
            AlterColumn("dbo.BasketLines", "SizeID", c => c.Int());
            AlterColumn("dbo.BasketLines", "ColourID", c => c.Int());
            RenameColumn(table: "dbo.BasketLines", name: "SizeID", newName: "Size_ID");
            RenameColumn(table: "dbo.BasketLines", name: "ColourID", newName: "Colour_ID");
            CreateIndex("dbo.BasketLines", "Size_ID");
            CreateIndex("dbo.BasketLines", "Colour_ID");
            AddForeignKey("dbo.BasketLines", "Size_ID", "dbo.Sizes", "ID");
            AddForeignKey("dbo.BasketLines", "Colour_ID", "dbo.Colours", "ID");
        }
    }
}
