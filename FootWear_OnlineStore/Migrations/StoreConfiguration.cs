namespace FootWear_OnlineStore.Migrations.StoreConfiguration
{
    using FootWear_OnlineStore.Models;
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class StoreConfiguration : DbMigrationsConfiguration<FootWear_OnlineStore.OSDB.StoreContext>
    {
        public StoreConfiguration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(FootWear_OnlineStore.OSDB.StoreContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data.
            var categories = new List<Category>
            {
                new Category { Name = "Mens Sport Shoes" },
                new Category { Name = "Womens Sport Shoes" },
            };
            categories.ForEach(c => context.Categories.AddOrUpdate(p => p.Name, c));
            context.SaveChanges();

            var colours = new List<Colour>
            {
                new Colour { Name = "White/White" },
                new Colour { Name = "Black/Black" },
                new Colour { Name = "White/Blue" },
                new Colour { Name = "Blue/Black" },
            };
            colours.ForEach(c => context.Colours.AddOrUpdate(p => p.Name, c));
            context.SaveChanges();

            var shoeSizes = new List<Size>
            {
                new Size { Region = "EN", Measure = 8 },
                new Size { Region = "EN", Measure = 9 },
                new Size { Region = "EN", Measure = 10 },
                new Size { Region = "EN", Measure = 11 }
            };

            shoeSizes.ForEach(c => context.Sizes.AddOrUpdate(p => p.Measure, c));
            context.SaveChanges();

            var images = new List<ProductImage>
            {
                new ProductImage { FileName="photo-Casual Lite Nike_1.jpg" },
                new ProductImage { FileName="photo-Casual Lite Nike_2.jpg" },
                new ProductImage { FileName="photo-Casual Lite Nike_3.jpg" },
                new ProductImage { FileName="photo-Casual Nike_1.jpg" },
                new ProductImage { FileName="photo-Casual Nike_2.jpg" },
                new ProductImage { FileName="photo-Nike Air_1.jpg" },
                new ProductImage { FileName="photo-Nike Air_2.jpg" },
                new ProductImage { FileName="photo-Nike Lite Air_1.jpg" },
                new ProductImage { FileName="photo-Nike Lite Air_2.jpg" },
                new ProductImage { FileName="photo-Sport Lite Nike_1.jpg" },
                new ProductImage { FileName="photo-Sport Lite Nike_2.jpg" },
                new ProductImage { FileName="photo-Sport Nike_1.jpg" }
            };
            images.ForEach(c => context.ProductImages.AddOrUpdate(p => p.FileName, c));
            context.SaveChanges();

            var products = new List<Product>
            {
                new Product { Name = "Nike Air",
                    Description =" Sports shoe with cushion sole",
                    Price=128.99M,
                    CategoryID =categories.Single( c => c.Name == "Mens Sport Shoes").ID
                },
                new Product { Name = "Nike Lite Air",
                    Description =" Soft cushion sole sports shoe",
                    Price=120.99M,
                    CategoryID =categories.Single( c => c.Name == "Womens Sport Shoes").ID
                },
                new Product { Name = "Casual Nike",
                    Description =" A shoe with light top upper",
                    Price=99.95M,
                    CategoryID =categories.Single( c => c.Name == "Mens Sport Shoes").ID
                },
                new Product { Name = "Casual Lite Nike",
                    Description ="A shoe with light top upper and comfortable heels",
                    Price=99.95M,
                    CategoryID =categories.Single( c => c.Name == "Womens Sport Shoes").ID
                },
                new Product { Name = "Sport Nike",
                    Description ="A shoe for atheltic sports",
                    Price=169.95M,
                    CategoryID =categories.Single( c => c.Name == "Womens Sport Shoes").ID
                },
                new Product { Name = "Sport Lite Nike",
                    Description ="A shoe for sports with light top upper and comfortable bottom heels",
                    Price=114.95M,
                    CategoryID =categories.Single( c => c.Name == "Womens Sport Shoes").ID
                },
            };
            products.ForEach(c => context.Products.AddOrUpdate(p => p.Name, c));
            context.SaveChanges();


            var imageMappings = new List<ProductImageMapping>
            {
                new ProductImageMapping { ID = 16, ProductImageID= images.Single(i => i.FileName ==
                    "photo-Nike Air_1.jpg").ID, ProductID = products.Single( c=> c.Name == "Nike Air").ID, ImageNumber = 0
                },
                new ProductImageMapping { ID = 17, ProductImageID= images.Single(i => i.FileName ==
                    "photo-Nike Air_2.jpg").ID, ProductID = products.Single( c=> c.Name == "Nike Air").ID, ImageNumber = 1
                },
                new ProductImageMapping { ID = 18, ProductImageID= images.Single(i => i.FileName ==
                    "photo-Nike Lite Air_1.jpg").ID, ProductID = products.Single( c=> c.Name == "Nike Lite Air").ID, ImageNumber = 0
                },
                new ProductImageMapping { ID = 19, ProductImageID= images.Single(i => i.FileName ==
                    "photo-Nike Lite Air_2.jpg").ID,ProductID = products.Single( c=> c.Name == "Nike Lite Air").ID, ImageNumber = 1
                },
                new ProductImageMapping { ID = 20, ProductImageID= images.Single(i => i.FileName ==
                    "photo-Casual Nike_1.jpg").ID, ProductID = products.Single( c=> c.Name == "Casual Nike").ID, ImageNumber = 0
                },
                new ProductImageMapping { ID = 21, ProductImageID= images.Single(i => i.FileName ==
                    "photo-Casual Nike_2.jpg").ID, ProductID = products.Single( c=> c.Name == "Casual Nike").ID, ImageNumber = 1
                },
                new ProductImageMapping { ID = 22, ProductImageID= images.Single(i => i.FileName ==
                    "photo-Casual Lite Nike_1.jpg").ID, ProductID = products.Single( c=> c.Name == "Casual Lite Nike").ID, ImageNumber = 0
                },
                new ProductImageMapping { ID = 23, ProductImageID= images.Single(i => i.FileName ==
                    "photo-Casual Lite Nike_2.jpg").ID, ProductID = products.Single( c=> c.Name == "Casual Lite Nike").ID, ImageNumber = 1
                },
                new ProductImageMapping { ID = 24, ProductImageID= images.Single(i => i.FileName ==
                    "photo-Casual Lite Nike_3.jpg").ID, ProductID = products.Single( c=> c.Name == "Casual Lite Nike").ID, ImageNumber = 2
                },
                new ProductImageMapping { ID = 25, ProductImageID= images.Single(i => i.FileName ==
                    "photo-Sport Nike_1.jpg").ID, ProductID = products.Single( c=> c.Name == "Sport Nike").ID, ImageNumber = 0
                },
                new ProductImageMapping { ID = 26, ProductImageID= images.Single(i => i.FileName ==
                    "photo-Sport Lite Nike_1.jpg").ID, ProductID = products.Single( c=> c.Name == "Sport Lite Nike").ID, ImageNumber = 0
                },
                new ProductImageMapping { ID = 27, ProductImageID= images.Single(i => i.FileName ==
                    "photo-Sport Lite Nike_2.jpg").ID, ProductID = products.Single( c=> c.Name == "Sport Lite Nike").ID, ImageNumber = 1
                }
            };
            imageMappings.ForEach(c => context.ProductImageMappings.AddOrUpdate(im => im.ProductImageID, c));
            context.SaveChanges();

            var orders = new List<Order>
            {
                new Order { DeliveryAddress = new Address { AddressLine1="1 Some Street",
                    Town="Town1", County="County", Postcode="PostCode" }, TotalPrice=99.95M,
                    UserID="admin@footwearstore.com", DateCreated=new DateTime(2018, 1, 1) ,
                    DeliveryName="Admin" },
                new Order { DeliveryAddress = new Address { AddressLine1="1 Some Street",
                    Town="Town1", County="County", Postcode="PostCode" }, TotalPrice=169.95M,
                    UserID="admin@footwearstore.com", DateCreated=new DateTime(2018, 1, 2) ,
                    DeliveryName="Admin" },
                new Order { DeliveryAddress = new Address { AddressLine1="1 Some Street",
                    Town="Town1", County="County", Postcode="PostCode" }, TotalPrice=114.95M,
                    UserID="admin@footwearstore.com", DateCreated=new DateTime(2018, 1, 3) ,
                    DeliveryName="Admin" },
                new Order { DeliveryAddress = new Address { AddressLine1="1 Some Street",
                    Town="Town1", County="County", Postcode="PostCode" }, TotalPrice=128.99M,
                    UserID="admin@footwearstore.com", DateCreated=new DateTime(2018, 1, 4) ,
                    DeliveryName="Admin" },
                new Order { DeliveryAddress = new Address { AddressLine1="1 Some Street",
                    Town="Town1", County="County", Postcode="PostCode" }, TotalPrice=99.95M,
                    UserID="admin@footwearstore.com", DateCreated=new DateTime(2018, 1, 5) ,
                    DeliveryName="Admin" }
            };
            orders.ForEach(c => context.Orders.AddOrUpdate(o => o.DateCreated, c));
            context.SaveChanges();

            var orderLines = new List<OrderLine>
            {
                new OrderLine { OrderID = 1, ProductID = products.Single( c=> c.Name == "Casual Lite Nike").ID, ProductName="Casual Lite Nike (White/White EN-8)",
                    Quantity = 1, UnitPrice=products.Single( c=> c.Name == "Casual Lite Nike").Price },
                new OrderLine { OrderID = 2, ProductID = products.Single( c=> c.Name == "Sport Nike").ID, ProductName="Sport Nike (White/Blue EN-8)",
                    Quantity =1, UnitPrice=products.Single( c=> c.Name =="Sport Nike").Price },
                new OrderLine { OrderID = 3, ProductID = products.Single( c=> c.Name == "Sport Lite Nike").ID, ProductName="Sport Lite Nike (Red/White EN-8)",
                    Quantity =1, UnitPrice=products.Single( c=> c.Name == "Sport Lite Nike").Price },
                new OrderLine { OrderID = 4, ProductID = products.Single( c=> c.Name == "Nike Air").ID, ProductName="Nike Air (White/White EN-8)",
                    Quantity =1, UnitPrice=products.Single( c=> c.Name == "Nike Air").Price },
                new OrderLine { OrderID = 5, ProductID = products.Single( c=> c.Name == "Casual Nike").ID, ProductName="Casual Nike (Black/Black EN-8)",
                    Quantity =1, UnitPrice=products.Single( c=> c.Name == "Casual Nike").Price }
            };
            orderLines.ForEach(c => context.OrderLines.AddOrUpdate(ol => ol.OrderID, c));
            context.SaveChanges();

    }
}
}
