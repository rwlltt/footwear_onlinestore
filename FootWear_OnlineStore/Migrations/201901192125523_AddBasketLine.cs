namespace FootWear_OnlineStore.Migrations.StoreConfiguration
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddBasketLine : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.BasketLines",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        BasketID = c.String(),
                        ProductID = c.Int(nullable: false),
                        Quantity = c.Int(nullable: false),
                        DateCreated = c.DateTime(nullable: false),
                        Colour_ID = c.Int(),
                        Size_ID = c.Int(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Colours", t => t.Colour_ID)
                .ForeignKey("dbo.Products", t => t.ProductID, cascadeDelete: true)
                .ForeignKey("dbo.Sizes", t => t.Size_ID)
                .Index(t => t.ProductID)
                .Index(t => t.Colour_ID)
                .Index(t => t.Size_ID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.BasketLines", "Size_ID", "dbo.Sizes");
            DropForeignKey("dbo.BasketLines", "ProductID", "dbo.Products");
            DropForeignKey("dbo.BasketLines", "Colour_ID", "dbo.Colours");
            DropIndex("dbo.BasketLines", new[] { "Size_ID" });
            DropIndex("dbo.BasketLines", new[] { "Colour_ID" });
            DropIndex("dbo.BasketLines", new[] { "ProductID" });
            DropTable("dbo.BasketLines");
        }
    }
}
